/*******************************************************************************
 *  Copyright © 2017 - 2022 Leipzig University (Database Research Group)
 *  
 *  Licensed under the Apache License, Version 2.0 (the "License"). You may not
 *  use this file except in compliance with the License. You may obtain a copy of
 *  the License at http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 *  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 *  License for the specific language governing permissions and limitations under 
 * the License.
 *******************************************************************************/
package de.uni_leipzig.dbs.pprl.primat.dataowner.encoding.bloomfilter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.uni_leipzig.dbs.pprl.primat.common.extraction.FeatureExtraction;
import de.uni_leipzig.dbs.pprl.primat.common.model.Record;
import de.uni_leipzig.dbs.pprl.primat.common.model.attributes.QidAttribute;
import de.uni_leipzig.dbs.pprl.primat.dataowner.encoding.Encoder;
import de.uni_leipzig.dbs.pprl.primat.dataowner.encoding.bloomfilter.hardening.BloomFilterHardener;
import de.uni_leipzig.dbs.pprl.primat.dataowner.encoding.bloomfilter.hashing.HashingMethod;


/**
 * Bloom-filter-based encoding of sensitive information.
 * 
 * The basic approach is that relevant attribute values are transformed into a
 * set of features. These features are (hash) mapped into one or several Bloom
 * filters, e.g. one Bloom filter for each attribute value or one Bloom filter
 * for <underline>all</underline> attribute values.
 * 
 * @author mfranke
 *
 */
public final class BloomFilterEncoder implements Encoder {

	private final List<BloomFilterDefinition> bfDefs;

	/**
	 * Constructs a new encoder.
	 * 
	 * @param bfDefs a list of {@link BloomFilterDefinition} objects that define
	 *               how and how many Bloom filters are constructed.
	 */
	public BloomFilterEncoder(List<BloomFilterDefinition> bfDefs) {
		this.bfDefs = bfDefs;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Record encode(Record record) {
		final Record encodedRec = new Record();
		encodedRec.setIdAttribute(record.getIdAttribute());
		encodedRec.setGlobalIdAttribute(record.getGlobalIdAttribute());
		encodedRec.setPartyAttribute(record.getPartyAttribute());;

		for (final BloomFilterDefinition bfDef : bfDefs) {
			this.buildBloomFilter(bfDef, record, encodedRec);
		}

		return encodedRec;
	}

	private void buildBloomFilter(BloomFilterDefinition bfDef, Record record, Record encodedRecord) {
		final int bfLength = bfDef.getBfLength();
		final BloomFilter bf = new BloomFilter(bfLength);

		final Set<Integer> positions = this.getBloomFilterPositions(bfDef, record);
		bf.setPositions(positions);
		
		final BloomFilterHardener hardener = bfDef.getHardener();
		final QidAttribute<?> hardBf = hardener.harden(bf);
		encodedRecord.addQidAttribute(hardBf);
	}

	private Set<Integer> getBloomFilterPositions(BloomFilterDefinition bfDef, Record record) {
		final List<BloomFilterExtractorDefinition> exDefs = bfDef.getFeatureExtractors();
		final HashingMethod hashingMethod = bfDef.getHashingMethod();
		final Set<Integer> positions = new HashSet<>();

		final String recordSalt;

		if (bfDef.hasRecordSalt()) {
			final int recSaltCol = bfDef.getRecordSaltColumn();
			recordSalt = record.getQidAttribute(recSaltCol).getStringValue();
		}
		else {
			recordSalt = "";
		}

		for (final BloomFilterExtractorDefinition exDef : exDefs) {
			final FeatureExtraction featEx = new FeatureExtraction(exDef);
			final Set<String> features = new HashSet<String>(featEx.getFeatures(record));			
			final int hashFunctions = exDef.getNumberOfHashFunctions();

			final String attributeSalt = exDef.getSalt();
			final String salt = attributeSalt + recordSalt;

			hashingMethod.setSalt(salt);
			final Set<Integer> currentPositionSet = hashingMethod.hash(features, hashFunctions);
			positions.addAll(currentPositionSet);
		}

		return positions;
	}

	public List<String> getSchema() {
		final List<String> schema = new ArrayList<>();
		schema.add("ID");
		schema.add("GID");
		schema.add("PARTY");

		for (final BloomFilterDefinition def : this.bfDefs) {
			final String name = def.getName();
			schema.add(name);
		}

		return schema;
	}
}